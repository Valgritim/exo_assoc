package projet.test;

import java.util.Date;

import projet.Profil;
import projet.User;

public class Test {

	public static void main(String[] args) {
		
		Profil [] profils = new Profil[3];
		profils[0] = new Profil("CP","Chef de projet");
		profils[1] = new Profil("MN","Manager");
		profils[2] = new Profil("DP","Développeur");
		
		User user1 = new User("Albert","Einstein", new Date("01/25/1912"), profils[1]);
		User user2 = new User("Eric","Charles", new Date("07/26/1980"), profils[1]);
		User user3 = new User("Bill","Gates", new Date("10/02/1977"), profils[2]);
		
		System.out.println(user1);
		System.out.println(user2);
		System.out.println(user3);
	}

}
