package projet;

import java.text.SimpleDateFormat;
import java.util.Date;

public class User {
	
	 private int id;
	 private String firstname;
	 private String lastname;
	 private Date birthdate;
	 private Profil profil;
	 private static int count;
	 
	public User(String firstname, String lastname, Date birthdate, Profil profil) {
		super();
		this.id = ++count;
		this.firstname = firstname;
		this.lastname = lastname;
		this.birthdate = birthdate;
		this.profil = profil;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public Date getBirthdate() {
		return birthdate;
	}

	public void setBirthdate(Date birthdate) {
		this.birthdate = birthdate;
	}

	public Profil getProfil() {
		return profil;
	}

	public void setProfil(Profil profil) {
		this.profil = profil;
	}

	@Override
	public String toString() {
		SimpleDateFormat df= new SimpleDateFormat("d MMMM yyyy");
		String date = df.format(this.getBirthdate());
		return "User [id=" + id + ", firstname=" + firstname + ", lastname=" + lastname + ", birthdate=" + date
				+ ", profil=" + profil + "]";
	}
	
	
	 
	 

}
